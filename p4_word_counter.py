# 第 0004 题：任一个英文的纯文本文件，统计其中的单词出现的个数。
import traceback


def count(file_path):

    result = dict()
    with open(file_path, 'r') as file:
        while True:
            line = file.readline()

            if not line:
                break

            for word in line.split(" "):
                result[word] = result.get(word, 0) + 1

    print(result)


if __name__ == '__main__':
    count('/Users/ningge/Desktop/test.txt')

