# 第 0005 题：你有一个目录，装了很多照片，把它们的尺寸变成都不大于 iPhone5 分辨率的大小。
from PIL import Image
import os


def process(path, out_path):
    list_dirs = os.walk(path)

    size = (640, 1136)

    for root, dirs, files in list_dirs:
        for f in files:

            name, type = os.path.splitext(f)
            if type == '.jpg':
                with Image.open(os.path.join(path, f)) as im:

                    ratio = min(size[0] / im.size[0], size[1] / im.size[1])
                    new_size = (ratio * im.size[0], ratio * im.size[1])

                    im.thumbnail(new_size, Image.ANTIALIAS)
                    im.save(out_path + 'new_' + name, "JPEG")


process('/Users/ningge/Downloads/乱七八糟的照片/妹汁', '/Users/ningge/Desktop/resize_result/')